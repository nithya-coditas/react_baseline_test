import React, { useState } from "react";

function App(props) {
  const [pokemon, setPokemon] = useState([]);

  function catchPokemon(target) {
    const { name } = target;
    setPokemon([name, ...pokemon]);
  }
  return (
    <>
      <h1>Pokemons</h1>
      {props.pokemons.map((pokemon) => (
        <button name={pokemon} onClick={catchPokemon}>{pokemon}</button>
      ))}

      <h1>Caught Pokemons</h1>
      {pokemon.map((poki) => (
        <button>{poki}</button>
      ))}
    </>
  );
}

export default App;
