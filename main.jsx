import React from "react";
import App from "./app";

export const getPokemons = async () => {
  const response = await axios.get(
    "https://e672-47-247-159-210.ngrok-free.app/pokemons",
    { headers: { "ngrok-skip-browser-warning": "skip-browser-warning" } }
  );
  return response?.data?.data;
};

const pokemons = getPokemons();

ReactDOM.createRoot(document.getElementById("root")).render(
  <App pokemons={pokemons} />
);
